> Calculate the area of triangle with sides a = 4.5, b = 5.9 and c = 9.  
Print calculated value with precision = 2

#### Solution
Using the *Heron's formula* we calculate the area of triangle  
and print it with precision 2 using f-strings.

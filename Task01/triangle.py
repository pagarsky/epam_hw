import math


def triangle_area(a: float, b: float, c: float) -> float:
    p = (a + b + c) / 2
    return math.sqrt(p * (p-a) * (p-b) * (p-c))


if __name__ == '__main__':
    area = triangle_area(4.5, 5.9, 9)
    print(f"{area:.2f}")

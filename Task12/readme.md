### Task 1
1a. Invoke pwd to see your current working directory (there should be your home directory).  
1b. Collect output of these commands  
• ls -l /  
• ls  
• ls ~  
• ls -l  
• ls -a  
• ls -la  
• ls -lda ~  
Note differences between produced outputs. Describe (in few words) purposes of these commands.

![Task 1](./task1.png)
![Task 1b1](./task1b1.png)
![Task 1b2](./task1b2.png)
![Task 1b3](./task1b3.png)
![Task 1b4](./task1b4.png)
![Task 1b5](./task1b5.png)
![Task 1b6](./task1b6.png)
![Task 1b7](./task1b7.png)

'ls' command is used to get content of directory. It can be used with different parameters,  
like '-l' to get output in long format or '-a' to sho hidden files as well.  
We can provide directory to check it's content, like '~' in screenshots above.

1c. Execute and describe the following commands (store the output, if any):  
• mkdir test  
• cd test  
• pwd  
• touch test.txt  
• ls -l test.txt  
• mkdir test2  
• mv test.txt test2  
• cd test2  
• ls  
• mv test.txt test2.txt  
• ls  
• cp test2.txt ..  
• cd ..  
• ls  
• rm test2.txt  
• rmdir test2  

![Task 1c1](./task1c1.png)
![Task 1c2](./task1c2.png)
![Task 1c3](./task1c3.png)
![Task 1c4](./task1c4.png)
![Task 1c5](./task1c5.png)

1d. Execute and describe the difference  
• cat /etc/fstab  
• less /etc/fstab  
• more /etc/fstab  

![Task1d1](./task1d1.png)
![Task1d2](./task1d2.png)
![Task1d3](./task1d3.png)

'cat' command is used to concatenate and show files content  
more is file viewer which provides per-page access  
'less - opposite of more' (Quote - bash manual). More high-level tool to view files

1e. Look through man pages of the listed above commands.

### Task 2

2a. Discovering soft and hard links. Comment on results of these commands (place the output into your report):  
• cd  
• mkdir test  
• cd test  
• touch test1.txt  
• echo “test1.txt” > test1.txt  
• ls -l . (a hard link)  
• ln test1.txt test2.txt  
• ls -l . // (pay attention to the number of links to test1.txt and test2.txt)  
• echo “test2.txt” > test2.txt  
• cat test1.txt test2.txt  
• rm test1.txt  
• ls -l . // (now a soft link)  
• ln -s test2.txt test3.txt  
• ls -l . //(pay attention to the number of links to the created files)  
• rm test2.txt; ls -l .  

![Task2](./task2.png)

2b. Dealing with chmod.  
An executable script. Open your favorite editor and put these lines into a file  
• #!/bin/bash  
• echo “Drugs are bad MKAY?”  
Give name “script.sh” to the script and call to  
• chmod +x script.sh  
Then you are ready to execute the script:  
• ./script.sh

![Task2b](./task2b.png)
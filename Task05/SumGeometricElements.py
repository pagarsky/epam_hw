def SumGeometricElements(a1: float, t: float, alim: float = 20) -> float:
    lst = []
    while a1 > alim:
        lst.append(a1)
        a1 *= t

    return sum(lst)


if __name__ == '__main__':
    a1, t, alim = 100, 0.5, 20
    print(SumGeometricElements(a1, t, alim))

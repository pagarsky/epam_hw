from operator import mul
from functools import reduce


def MultArithmeticElements(n: float, a1: float, t: float):
    def float_range(_n, _a1, _t):
        count = 0
        while count < _n:
            yield _a1
            _a1 += _t
            count += 1

    return reduce(mul, float_range(n, a1, t))


if __name__ == '__main__':
    a1, t, n = 5, 3, 4
    print(MultArithmeticElements(n, a1, t))

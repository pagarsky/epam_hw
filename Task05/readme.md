### Task 1 (isSorted)

Create function IsSorted, determining whether a given array of integer values of arbitrary length is sorted in a given order (the order is set up by enum value SortOrder).  
Array and sort order are passed by parameters. Function does not change the array.

```python
from enum import Enum
from operator import lt, gt


class SortOrder(Enum):
    ascending = lt
    descending = gt


def isSorted(array: list, order: SortOrder) -> bool:
    op = order.value
    i = 1
    while i < len(array):
        if op(array[i], array[i-1]):
            return False
        i += 1

    return True


if __name__ == '__main__':
    arr = [1, 2, 3, 4, 5, 6]
    ar2 = [6, 5, 4, 3, 2, 1]
    print(isSorted(arr, SortOrder.ascending))
```


### Task 2 (Transform)
Create function Transform, replacing the value of each element of an integer array with the sum of this element value and its index,  
only if the given array is sorted in the given order (the order is set up by enum value SortOrder).  
Array and sort order are passed by parameters.  
To check, if the array is sorted, the function IsSorted from the Task 1 is called.

```python
from Task05.isSorted import isSorted, SortOrder


def Transform(array: list, order: SortOrder) -> None:
    if not isSorted(array, order):
        return

    for i in range(len(array)):
        array[i] += i


if __name__ == '__main__':
    test1 = [5, 17, 24, 88, 33, 2]
    test2 = [15, 10, 3]

    Transform(test1, SortOrder.ascending)
    print(test1)

    Transform(test2, SortOrder.ascending)
    print(test2)

    Transform(test2, SortOrder.descending)
    print(test2)
```

### Task 3 (MultArithmeticElements)

Create function MultArithmeticElements, which determines the multiplication of the first n elements of arithmetic progression of real numbers with a given initial element of progression a1 and progression step t.
a<sub>n</sub> is calculated by the formula (a<sub>n+1</sub> = a<sub>n</sub> + 1).

```python
from operator import mul
from functools import reduce


def MultArithmeticElements(n: float, a1: float, t: float):
    def float_range(_n, _a1, _t):
        count = 0
        while count < _n:
            yield _a1
            _a1 += _t
            count += 1

    return reduce(mul, float_range(n, a1, t))


if __name__ == '__main__':
    a1, t, n = 5, 3, 4
    print(MultArithmeticElements(n, a1, t))
```

### Task 4 (SumGeometricElements)
Create function SumGeometricElements,  
determining the sum of the first elements of a decreasing geometric progression of real numbers with a given initial element of a progression aj and a given progression step t, while the last element must be greater than a given alim.  
a<sub>n</sub> is calculated by the formula (a<sub>n+1</sub> = a<sub>n</sub> * t), 0 < t < 1.

```python

def SumGeometricElements(a1: float, t: float, alim: float = 20) -> float:
    lst = []
    while a1 > alim:
        lst.append(a1)
        a1 *= t

    return sum(lst)


if __name__ == '__main__':
    a1, t, alim = 100, 0.5, 20
    print(SumGeometricElements(a1, t, alim))
```
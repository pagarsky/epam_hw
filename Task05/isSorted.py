from enum import Enum
from operator import lt, gt


class SortOrder(Enum):
    ascending = lt
    descending = gt


def isSorted(array: list, order: SortOrder) -> bool:
    op = order.value
    i = 1
    while i < len(array):
        if op(array[i], array[i-1]):
            return False
        i += 1

    return True


if __name__ == '__main__':
    arr = [1, 2, 3, 4, 5, 6]
    ar2 = [6, 5, 4, 3, 2, 1]
    print(isSorted(arr, SortOrder.ascending))

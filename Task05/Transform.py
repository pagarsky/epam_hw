from Task05.isSorted import isSorted, SortOrder


def Transform(array: list, order: SortOrder) -> None:
    if not isSorted(array, order):
        return

    for i in range(len(array)):
        array[i] += i


if __name__ == '__main__':
    test1 = [5, 17, 24, 88, 33, 2]
    test2 = [15, 10, 3]

    Transform(test1, SortOrder.ascending)
    print(test1)

    Transform(test2, SortOrder.ascending)
    print(test2)

    Transform(test2, SortOrder.descending)
    print(test2)

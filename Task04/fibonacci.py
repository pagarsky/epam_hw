def fibonacci_sum(n: int) -> int:
    if n == 0:
        return 0

    numbers = [0, 1]
    for _ in range(n-1):
        numbers.append(numbers[-1] + numbers[-2])

    return sum(numbers)


if __name__ == '__main__':
    n = int(input('Please enter n: '))
    print(fibonacci_sum(n))

from typing import List


def transpose(matrix: List) -> List[List]:
    cpy = []
    rows, cols = len(matrix), len(matrix[0])

    for col in range(cols):
        cpy.append([])
        for row in range(rows):
            cpy[-1].append(matrix[row][col])

    return cpy


if __name__ == '__main__':
    matrix = [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 0, 1, 2]
    ]
    print(transpose(matrix))

def binary(n: int) -> str:
    lst = []

    while n:
        lst.append(str(n % 2))
        n //= 2

    return ''.join(reversed(lst))


if __name__ == '__main__':
    n = int(input("Enter a number: "))
    b = binary(n)

    print(f'Number is {n}, binary representation is {b}, sum is {sum([int(x) for x in b])}')

### Task 1 (Transpose matrix)
* Write a script that can transpose any matrix

```python
from typing import List


def transpose(matrix: List) -> List[List]:
    cpy = []
    rows, cols = len(matrix), len(matrix[0])

    for col in range(cols):
        cpy.append([])
        for row in range(rows):
            cpy[-1].append(matrix[row][col])

    return cpy


if __name__ == '__main__':
    matrix = [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 0, 1, 2]
    ]
    print(transpose(matrix))
```

### Task 2 (Calculate Factorial)
* Calculate tha factorial for positive number <b>n</b>

```python
from functools import reduce


def factorial(n: int) -> int:
    return reduce(lambda x, y: x*y, range(1, n+1))


if __name__ == '__main__':
    print(factorial(5))
```

### Task 3 (Fibonacci sum)
* For a positive integer <b>n</b> calculate the result value,  
which is equal to the sum of the first <b>n</b> Fibonacci numbers

```python
def fibonacci_sum(n: int) -> int:
    if n == 0:
        return 0

    numbers = [0, 1]
    for _ in range(n-1):
        numbers.append(numbers[-1] + numbers[-2])

    return sum(numbers)


if __name__ == '__main__':
    n = int(input('Please enter n: '))
    print(fibonacci_sum(n))
```

### Task 4 (Binary represenation)
* For a positive integer <b>n</b> calculate the result value,  
which is equal to the sum of "1" if the binary representation of n

```python
def binary(n: int) -> str:
    lst = []

    while n:
        lst.append(str(n % 2))
        n //= 2

    return ''.join(reversed(lst))


if __name__ == '__main__':
    n = int(input("Enter a number: "))
    b = binary(n)

    print(f'Number is {n}, binary representation is {b}, sum is {sum([int(x) for x in b])}')
```
import gc
from typing import Union, Any
from collections.abc import Iterable


class CustomList(Iterable):
    class Node:
        def __init__(self, val):
            self.val = val
            self.next = None

        def __str__(self) -> str:
            temp = self
            lst = []
            while temp is not None:
                lst.append(str(temp.val))
                temp = temp.next

            return ' -> '.join(lst)

    class Iterator:
        def __init__(self, node, length):
            self.current = node
            self.length = length

        def __iter__(self):
            return self.current.val

        def __next__(self):
            if self.current is None:
                raise StopIteration
            val = self.current.val
            self.current = self.current.next
            return val

    def __init__(self, *args):
        self.head = None
        self.length = 0

        if len(args) == 1 and isinstance(args[0], Iterable):
            for item in args[0]:
                self.add(item)
        else:
            for item in args:
                self.add(item)

    def __len__(self) -> int:
        return self.length

    def __getitem__(self, key) -> Union[Any, Node]:
        if isinstance(key, int):
            if key >= self.length:
                raise IndexError
            if key < 0:
                key += self.length
            return self.find(key).val
        elif isinstance(key, slice):
            dummy = self.Node(-1)
            res = dummy

            # not very clean way to do it but it works
            start = key.start if key.start else 0
            stop = key.stop if key.stop else self.length
            step = key.step if key.step else 1

            # find start of a slice
            temp = self._jump(self.head, start)
            index = start

            # create slice
            while index < stop:
                res.next = self.Node(temp.val)
                res = res.next
                temp = self._jump(temp, step)
                index += step

            return dummy.next
        else:
            raise TypeError

    def __setitem__(self, key, value) -> None:
        if key >= self.length:
            raise IndexError

        if isinstance(key, int):
            self.find(key).val = value
        elif isinstance(key, slice):
            # not very clean way to do it but it works
            start = key.start if key.start else 0
            stop = key.stop if key.stop else self.length
            step = key.step if key.step else 1

            # find start of a slice
            temp = self._jump(self.head, start)
            index = start

            # set value to a slice
            while index < stop:
                temp.val = value
                temp = self._jump(temp, step)
                index += step
        else:
            raise TypeError

    def add(self, item) -> None:
        if self.head is None:
            self.head = self.Node(item)
            self.length += 1
            return

        temp = self._jump(self.head, self.length - 1)

        temp.next = self.Node(item)
        self.length += 1

    def remove(self, index: int) -> None:
        if index >= self.length:
            raise IndexError
        if not isinstance(index, int):
            raise TypeError

        node = self._jump(self.head, index - 1)
        throw = node.next

        if throw is not None:
            node.next = throw.next
            throw.next = None

    def index(self, item) -> int:
        if self.head is None:
            raise ValueError

        index = 0
        node = self.head
        while index < self.length:
            if node.val == item:
                return index
            node = node.next
            index += 1
        else:
            raise ValueError

    def clear(self) -> None:
        # gc deletes all nodes as checked in main
        self.head = None
        self.length = 0

    def _jump(self, node: Node, index: int) -> Node:
        tmp = node
        while index:
            tmp = tmp.next
            index -= 1
        return tmp

    def find(self, index: int) -> Node:
        i = 0
        node = self.head
        while i < index:
            node = node.next
            i += 1
        return node

    def __iter__(self):
        self.iter = self.Iterator(self.head, self.length)
        return self.iter

    def __next__(self):
        try:
            return next(self.iter)
        except StopIteration:
            self.__delattr__('iter')
            raise StopIteration

    def __contains__(self, item) -> bool:
        node = self.head
        while node is not None:
            if node.val == item:
                return True
            node = node.next
        return False

    def __str__(self) -> str:
        node = self.head
        lst = []
        while node is not None:
            lst.append(str(node.val))
            node = node.next

        return ' -> '.join(lst)


if __name__ == '__main__':
    lst = [1, 2, 3, 4, 5, 6]
    custom = CustomList(*lst)

    print(f"__str__(): {custom}")
    print(f"__contains__(), 3: {3 in custom}, -1: {-1 in custom}")
    print(f"len(): {len(custom)}\n")

    print("indexing:")
    print(f"custom[2]: {custom[2]}, custom[-2]: {custom[-2]}, custom[2] = -1:")
    custom[2] = -1
    print(custom)
    print(f"slicing, custom[:5:2]:")
    print(custom[:5:2], '\n')

    print(f"index(4): {custom.index(4)}\n")

    custom.add(7)
    custom.add(8)
    custom.add(9)
    print("After add(7), add(8), add(9): ")
    print(custom, '\n')

    print("After remove(2), remove(7): ")
    custom.remove(2)
    custom.remove(7)
    print(custom, '\n')

    print(f"gc objects before clear(): {len(gc.get_objects())}")
    custom.clear()
    print(f"gc objects after clear(): {len(gc.get_objects())}")

    custom = CustomList([1, 2, 3, 4, 5])
    print(f"new list: {custom}, testing __iter__():")
    out = []
    for i in custom:
        out.append(i)
    print(out)

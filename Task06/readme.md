### Task 1 (Combine Dicts)
Implement a function, that receives changeable number of dictionaries (keys - letters, values - numbers)  
and combines them into one dictionary.
Dict values should be summarized in case of identical keys

```python
def combine_dicts(*args) -> dict:
    res = {}
    for _dict in args:
        for k, v in _dict.items():
            if k in res:
                res[k] += v
            else:
                res[k] = v
    return res


if __name__ == '__main__':
    a = {'a': 1, 'b': 1, 'c': 1}
    b = {'a': 2, 'e': 2, 'f': 2}
    c = {'a': 3, 'h': 3, 'i': 3}

    print(combine_dicts(a, b, c))
```

### Task 2(CustomList)
***
To create generic type CustomList ~ the list of values, which has length that is extended when new elements are added to the list.

CustomList is a collection — the list of values of random type, its size changes dynamically and there is a possibility to index list elements. Indexation in
the list starts with 0.
***
#### Solution
##### code in CustomList.py because it's ~200 lines
I implemented Singly-Linked list with all basic operations like:
* Creating of empty user list and the one based on elements set which should be passed to our list as unpacked iterable
* Adding, removing elements (methods add and remove)
* Operations with elements by index (basic indexing using [] and slicing)
* Clearing the list, receiving its length (method clear and built-in function len)
* Receiving link to linked elements list (method find)
* Generating exceptions (according to protocols)
* Receiving from numerator list for operator foreach (iter, next and internal Iterator class)

#### What's <i>NOT</i> done
* <b>Negative indexes for slices</b>  
Probably easy fix, but I can't see clean solution so that's how it is
* <b>Possible memory leak</b>  
Probably some dummy nodes left after add() but I <i>just can't see</i> it.  
clear() solves it, but that's not the way to do it, I think.

#### What can be done better
* <b>Internal class for Node</b>  
I don't actually know if I needed it at all, so probably I can rewrite this class clearer and better after lecture.
* <b>Unit tests in main instead of prints</b>  
Yeah, I could test all that code without this Neanderthal prints everywhere.
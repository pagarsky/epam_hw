def combine_dicts(*args) -> dict:
    res = {}
    for _dict in args:
        for k, v in _dict.items():
            if k in res:
                res[k] += v
            else:
                res[k] = v
    return res


if __name__ == '__main__':
    a = {'a': 1, 'b': 1, 'c': 1}
    b = {'a': 2, 'e': 2, 'f': 2}
    c = {'a': 3, 'h': 3, 'i': 3}

    print(combine_dicts(a, b, c))

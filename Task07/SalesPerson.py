from Task07 import Employee


class SalesPerson(Employee.Employee):

    def __init__(self, name: str, salary: int, percent: int):
        super().__init__(name, salary)
        self._percent = percent

    @property
    def percent(self) -> int:
        return self._percent

    def set_bonus(self, bonus) -> None:
        if self.percent > 100:
            super().set_bonus(2 * bonus)
        elif self.percent > 200:
            super().set_bonus(3 * bonus)
        else:
            super().set_bonus(bonus)

from typing import Union, Iterable

from Task07.Rectangle import Rectangle


class ArrayRectangles:

    def __init__(self, arg: Union[int, Iterable]):
        if isinstance(arg, int):
            self.rectangle_array = [None] * arg
            self.length = arg
        elif isinstance(arg, Iterable):
            self.rectangle_array = list(arg)
            self.length = len(self.rectangle_array)
        else:
            raise TypeError

    def add_rectangle(self, rect: Rectangle) -> bool:
        if not isinstance(rect, Rectangle):
            raise TypeError

        try:
            index = self.rectangle_array.index(None)
        except ValueError:
            return False

        self.rectangle_array.insert(index, rect)
        return True

    def number_max_area(self) -> int:
        def key(rect):
            if rect is None:
                return 0
            return rect.area()
        item = max(self.rectangle_array, key=key)
        return self.rectangle_array.index(item)

    def number_min_perimeter(self) -> int:
        def key(rect):
            if rect is None:
                return 0
            return rect.perimeter()
        item = min(self.rectangle_array, key=key)
        return self.rectangle_array.index(item)

    def number_square(self) -> int:
        number = 0

        for i in self.rectangle_array:
            if isinstance(i, Rectangle):
                if i.is_square():
                    number += 1

        return number

    def __str__(self):
        return f'[{", ".join(str(r) for r in self.rectangle_array)}]'


if __name__ == '__main__':
    rect = ArrayRectangles(5)
    print(rect)

    print(rect.add_rectangle(Rectangle(3, 3)))
    print(rect.add_rectangle(Rectangle(5, 4)))
    print(rect.add_rectangle(Rectangle(1, 2)))
    print(rect)
    print(rect.number_max_area())
    print(rect.number_min_perimeter())
    print(rect.number_square())

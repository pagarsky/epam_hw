### Task 1
Develop Rectangle and ArrayRectangle with predefined functionality.

```python
class Rectangle:

    def __init__(self, a: float, b: float = 5):
        self._side_a = abs(float(a))
        self._side_b = abs(float(b))

    def get_side_a(self) -> float:
        return self._side_a

    def get_side_b(self) -> float:
        return self._side_b

    def area(self) -> float:
        return self._side_a * self._side_b

    def perimeter(self) -> float:
        return 2 * self._side_a + 2 * self._side_b

    def is_square(self) -> bool:
        return self._side_a == self._side_b

    def replace_sides(self) -> None:
        self._side_a, self._side_b = self._side_b, self._side_a

    def __str__(self):
        return f'|{self._side_a}, {self._side_b}|'
```

```python
from typing import Union, Iterable

from Task07.Rectangle import Rectangle


class ArrayRectangles:

    def __init__(self, arg: Union[int, Iterable]):
        if isinstance(arg, int):
            self.rectangle_array = [None] * arg
            self.length = arg
        elif isinstance(arg, Iterable):
            self.rectangle_array = list(arg)
            self.length = len(self.rectangle_array)
        else:
            raise TypeError

    def add_rectangle(self, rect: Rectangle) -> bool:
        if not isinstance(rect, Rectangle):
            raise TypeError

        try:
            index = self.rectangle_array.index(None)
        except ValueError:
            return False

        self.rectangle_array.insert(index, rect)
        return True

    def number_max_area(self) -> int:
        def key(rect):
            if rect is None:
                return 0
            return rect.area()
        item = max(self.rectangle_array, key=key)
        return self.rectangle_array.index(item)

    def number_min_perimeter(self) -> int:
        def key(rect):
            if rect is None:
                return 0
            return rect.perimeter()
        item = min(self.rectangle_array, key=key)
        return self.rectangle_array.index(item)

    def number_square(self) -> int:
        number = 0

        for i in self.rectangle_array:
            if isinstance(i, Rectangle):
                if i.is_square():
                    number += 1

        return number

    def __str__(self):
        return f'[{", ".join(str(r) for r in self.rectangle_array)}]'


if __name__ == '__main__':
    rect = ArrayRectangles(5)
    print(rect)

    print(rect.add_rectangle(Rectangle(3, 3)))
    print(rect.add_rectangle(Rectangle(5, 4)))
    print(rect.add_rectangle(Rectangle(1, 2)))
    print(rect)
    print(rect.number_max_area())
    print(rect.number_min_perimeter())
    print(rect.number_square())
```

### Task 2
To create classes Employee, SalesPerson, Manager, Company with predefined functionality.

```python
class Employee:

    def __init__(self, name: str, salary: int):
        self._name = name
        self._salary = salary
        self._bonus = 0

    @property
    def name(self) -> str:
        return self._name

    @property
    def salary(self) -> int:
        return self._salary

    @property
    def bonus(self) -> int:
        return self._bonus

    def set_bonus(self, bonus) -> None:
        self._bonus = bonus

    def to_pay(self) -> int:
        return self.salary + self.bonus

    def __str__(self):
        return f'{self.name}, {self.salary}, {self.bonus}'
```

```python
from Task07 import Employee


class SalesPerson(Employee.Employee):

    def __init__(self, name: str, salary: int, percent: int):
        super().__init__(name, salary)
        self._percent = percent

    @property
    def percent(self) -> int:
        return self._percent

    def set_bonus(self, bonus) -> None:
        if self.percent > 100:
            super().set_bonus(2 * bonus)
        elif self.percent > 200:
            super().set_bonus(3 * bonus)
        else:
            super().set_bonus(bonus)
```

```python
from Task07 import Employee


class Manager(Employee.Employee):

    def __init__(self, name: str, salary: int, client_amount: int):
        super().__init__(name, salary)

        self._quantity = client_amount

    @property
    def quantity(self) -> int:
        return self._quantity

    def set_bonus(self, bonus) -> None:
        if self.quantity > 100:
            super().set_bonus(self.bonus + 500)
        elif self.quantity > 150:
            super().set_bonus(self.bonus + 1000)
        else:
            super().set_bonus(bonus)
```

```python
from typing import Iterable

from Task07.Employee import Employee
from Task07.SalesPerson import SalesPerson
from Task07.Manager import Manager


class Company:

    def __init__(self, it: Iterable[Employee]):
        employees = list(it)
        for emp in employees:
            if not isinstance(emp, Employee):
                raise TypeError(f"Cannot convert {emp} to Employee")

        self._employees = employees

    def give_everybody_a_bonus(self, company_bonus: int) -> None:
        for emp in self._employees:
            emp.set_bonus(company_bonus)

    def total_to_pay(self) -> int:
        return sum((emp.to_pay() for emp in self._employees))

    def name_max_salary(self) -> str:
        return max(self._employees, key=lambda x: x.to_pay()).name


if __name__ == '__main__':
    company = Company([
        Employee("Jameson", 200),
        Employee("Brook", 350),
        Employee("Mary", 300),
        Manager("Peter", 500, 140),
        SalesPerson("Thomas", 1200, 100)
    ])

    company.give_everybody_a_bonus(500)
    print(company.total_to_pay())
    print(company.name_max_salary())
```
from Task07 import Employee


class Manager(Employee.Employee):

    def __init__(self, name: str, salary: int, client_amount: int):
        super().__init__(name, salary)

        self._quantity = client_amount

    @property
    def quantity(self) -> int:
        return self._quantity

    def set_bonus(self, bonus) -> None:
        if self.quantity > 100:
            super().set_bonus(self.bonus + 500)
        elif self.quantity > 150:
            super().set_bonus(self.bonus + 1000)
        else:
            super().set_bonus(bonus)

class Employee:

    def __init__(self, name: str, salary: int):
        self._name = name
        self._salary = salary
        self._bonus = 0

    @property
    def name(self) -> str:
        return self._name

    @property
    def salary(self) -> int:
        return self._salary

    @property
    def bonus(self) -> int:
        return self._bonus

    def set_bonus(self, bonus) -> None:
        self._bonus = bonus

    def to_pay(self) -> int:
        return self.salary + self.bonus

    def __str__(self):
        return f'{self.name}, {self.salary}, {self.bonus}'

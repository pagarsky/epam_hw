class Rectangle:

    def __init__(self, a: float, b: float = 5):
        self._side_a = abs(float(a))
        self._side_b = abs(float(b))

    def get_side_a(self) -> float:
        return self._side_a

    def get_side_b(self) -> float:
        return self._side_b

    def area(self) -> float:
        return self._side_a * self._side_b

    def perimeter(self) -> float:
        return 2 * self._side_a + 2 * self._side_b

    def is_square(self) -> bool:
        return self._side_a == self._side_b

    def replace_sides(self) -> None:
        self._side_a, self._side_b = self._side_b, self._side_a

    def __str__(self):
        return f'|{self._side_a}, {self._side_b}|'


from typing import Iterable

from Task07.Employee import Employee
from Task07.SalesPerson import SalesPerson
from Task07.Manager import Manager


class Company:

    def __init__(self, it: Iterable[Employee]):
        employees = list(it)
        for emp in employees:
            if not isinstance(emp, Employee):
                raise TypeError(f"Cannot convert {emp} to Employee")

        self._employees = employees

    def give_everybody_a_bonus(self, company_bonus: int) -> None:
        for emp in self._employees:
            emp.set_bonus(company_bonus)

    def total_to_pay(self) -> int:
        return sum((emp.to_pay() for emp in self._employees))

    def name_max_salary(self) -> str:
        return max(self._employees, key=lambda x: x.to_pay()).name


if __name__ == '__main__':
    company = Company([
        Employee("Jameson", 200),
        Employee("Brook", 350),
        Employee("Mary", 300),
        Manager("Peter", 500, 140),
        SalesPerson("Thomas", 1200, 100)
    ])

    company.give_everybody_a_bonus(500)
    print(company.total_to_pay())
    print(company.name_max_salary())

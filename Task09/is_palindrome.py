def is_palindrome(string: str) -> bool:
    new_string = ''.join(ch.lower() for ch in string if ch.isalpha())

    lo, hi = 0, len(new_string) - 1
    for _ in range(len(new_string) // 2):
        if new_string[lo] != new_string[hi]:
            return False
        lo, hi = lo + 1, hi - 1
    return True


# Lazy solution :)

# def is_palindrome(string: str) -> bool:
#     new_string = ''.join(ch.lower() for ch in string if ch.isalpha())
#
#     return new_string == new_string[::-1]


if __name__ == '__main__':
    print(is_palindrome('''redivider'''))                               # True
    print(is_palindrome('''Was it a car or a cat i saw?'''))            # True
    print(is_palindrome('''Do geese see God?'''))                       # True
    print(is_palindrome('''Go hang a salami, I'm a lasagna hog'''))     # True
    print(is_palindrome('''Obviously not a palindrome'''))              # False
    print(is_palindrome('''And another one'''))                         # False

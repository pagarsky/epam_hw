def get_shortest_word(string: str) -> str:
    limit = len(string)
    i, words = 0, []

    while i < limit:
        j = i
        while string[j].isalpha():
            j += 1
        word = string[i:j]

        if word:
            words.append(word)
        i = j + 1

    return min(words, key=len)


if __name__ == '__main__':
    print(get_shortest_word('Python is simple \n and      effective!'))
    print(get_shortest_word('Any pythonista     like namespaces \n\n\n a lot, a?'))

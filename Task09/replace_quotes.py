def replace_quotes(string: str, to_single: bool = True) -> str:
    sep, quote = ('"', "'") if to_single else ("'", '"')

    lst = string.split(sep)

    return quote.join(lst)


if __name__ == '__main__':
    print(replace_quotes('''I'm the danger''', False))
    print(replace_quotes('''I"m the one who knocks'''))

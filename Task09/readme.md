### Task 1
Implement a function which receives a string and replaces all " symbols with ' and vise versa.

```python
def replace_quotes(string: str, to_single: bool = True) -> str:
    sep, quote = ('"', "'") if to_single else ("'", '"')

    lst = string.split(sep)

    return quote.join(lst)


if __name__ == '__main__':
    print(replace_quotes('''I'm the danger''', False))
    print(replace_quotes('''I"m the one who knocks'''))
```


### Task 2
Write a function that check whether a string is a palindrome or not.  
To check your implementation you can use strings from here (https://en.wikipedia.org/wiki/Palindrome#Famous_palindromes).

```python
def is_palindrome(string: str) -> bool:
    new_string = ''.join(ch.lower() for ch in string if ch.isalpha())

    lo, hi = 0, len(new_string) - 1
    for _ in range(len(new_string) // 2):
        if new_string[lo] != new_string[hi]:
            return False
        lo, hi = lo + 1, hi - 1
    return True


# Lazy solution :)

# def is_palindrome(string: str) -> bool:
#     new_string = ''.join(ch.lower() for ch in string if ch.isalpha())
#
#     return new_string == new_string[::-1]


if __name__ == '__main__':
    print(is_palindrome('''redivider'''))                               # True
    print(is_palindrome('''Was it a car or a cat i saw?'''))            # True
    print(is_palindrome('''Do geese see God?'''))                       # True
    print(is_palindrome('''Go hang a salami, I'm a lasagna hog'''))     # True
    print(is_palindrome('''Obviously not a palindrome'''))              # False
    print(is_palindrome('''And another one'''))                         # False
```

### Task 3
Implement a function get_shortest_word(s: str) -> str which returns the shortest word in the given string.  
The word can contain any symbols except whitespaces ( , \n, \t and so on).  
If there are multiple shortest words in the string with a same length return the word that occurs first.  
<b>Usage of any split functions is forbidden.</b>  
Example:  
\>>> get_shortest_word('Python is simple and effective!')  
'is'  
\>>> get_shortest_word('Any pythonistalike namespaces a lot, a? O')  
'a'
```python
def get_shortest_word(string: str) -> str:
    limit = len(string)
    i, words = 0, []

    while i < limit:
        j = i
        while string[j].isalpha():
            j += 1
        word = string[i:j]

        if word:
            words.append(word)
        i = j + 1

    return min(words, key=len)


if __name__ == '__main__':
    print(get_shortest_word('Python is simple \n and      effective!'))
    print(get_shortest_word('Any pythonista     like namespaces \n\n\n a lot, a?'))
```

### Task 4
Implement a bunch of functions which receive a changeable number of strings and return next parameters:
1) characters that appear in all strings
2) characters that appear in at least one string
3) characters that appear at least in two strings
4) characters of alphabet, that were not used in any string  

<b>Note: use string.ascii_lowercase for list of alphabet letters</b>  
\>>> pythontest_strings= ["hello", "world", "python", ]  
\>>> print(test_1_1(*strings))  
{'o'}  
\>>> print(test_1_2(*strings))  
{'d', 'e', 'h', 'l', 'n', 'o', 'p', 'r', 't', 'w', 'y'}  
\>>> print(test_1_3(*strings))  
{'h', 'l', 'o'}  
\>>> print(test_1_4(*strings))  
{'a', 'b', 'c', 'f', 'g', 'i', 'j', 'k', 'm', 'q', 's', 'u', 'v', 'x', 'z'}  

```python
from collections import Counter
import string


def in_all_strings(*args) -> list:
    return list(set.intersection(*[set(a) for a in args]))


def in_one_string(*args) -> list:
    return list(set(''.join(args)))


def in_two_strings(*args) -> list:
    return [item for item, count in Counter(''.join(args)).items() if count > 1]


def in_none(*args) -> list:
    alpha = set(string.ascii_lowercase)
    return list(alpha.difference(*[set(a) for a in args]))


if __name__ == '__main__':
    test_strings = ['hello', 'world', 'python']

    print(in_all_strings(*test_strings))
    print(in_one_string(*test_strings))
    print(in_two_strings(*test_strings))
    print(in_none(*test_strings))
```

### Task 5
Implement a function, that takes string as an argument and returns a dictionary, that contains letters of given string as keys and a number of their occurrence as values.  
\>>> pythonprint(count_letters("stringsample"))  
{'s': 2, 't': 1, 'r': 1, 'i': 1,'n': 1, 'g': 1, 'a': 1, 'm': 1,'p': 1, 'l': 1, 'e': 1}

```python
# from collections import Counter # :)
from collections import defaultdict


def count_letters(string: str):
    counter = defaultdict(int)

    for ch in string:
        counter[ch] += 1

    return dict(counter)


if __name__ == '__main__':
    print(count_letters("stringsample"))
    print(count_letters("sssaaaabb"))
    print(count_letters("stringssngrisgoiavrgsieaaafafafafafhvnsrntample"))
```
# from collections import Counter # :)
from collections import defaultdict


def count_letters(string: str):
    counter = defaultdict(int)

    for ch in string:
        counter[ch] += 1

    return dict(counter)


if __name__ == '__main__':
    print(count_letters("stringsample"))
    print(count_letters("sssaaaabb"))
    print(count_letters("stringssngrisgoiavrgsieaaafafafafafhvnsrntample"))

from collections import Counter
import string


def in_all_strings(*args) -> list:
    return list(set.intersection(*[set(a) for a in args]))


def in_one_string(*args) -> list:
    return list(set(''.join(args)))


def in_two_strings(*args) -> list:
    return [item for item, count in Counter(''.join(args)).items() if count > 1]


def in_none(*args) -> list:
    alpha = set(string.ascii_lowercase)
    return list(alpha.difference(*[set(a) for a in args]))


if __name__ == '__main__':
    test_strings = ['hello', 'world', 'python']

    print(in_all_strings(*test_strings))
    print(in_one_string(*test_strings))
    print(in_two_strings(*test_strings))
    print(in_none(*test_strings))

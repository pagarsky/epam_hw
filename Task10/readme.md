### Task 1
Sort the names in 'data/unsorted_names.txt' and write them to a new file called 'sorted_names/txt'.
```python
import sys


def sort_names(source: str, destination: str) -> None:
    try:
        with open(source, 'r') as file:
            names = file.readlines()
            names.sort()

        with open(destination, 'w') as dest:
            dest.writelines(names)

    except FileNotFoundError:
        print(f'No file found at {source} or {destination}')


if __name__ == '__main__':
    sort_names(sys.path[0] + '/data/unsorted_names.txt', sys.path[0] + '/data/sorted_names.txt')
```

### Task 2
Implement a function which search for most common words in the file.
* <b>NOTE:</b> remember about dots, commas, capital letters, etc.
```python
from collections import Counter
import sys


def most_common_words(filepath: str, number_of_words: int = 3) -> list:
    try:
        words = Counter()
        with open(filepath, 'r') as file:
            for line in file.readlines():
                words.update(word.strip('.,').lower() for word in line.split())

        return [tup[0] for tup in words.most_common(number_of_words)]
    except FileNotFoundError:
        print(f'File not found at {filepath}')


if __name__ == '__main__':
    print(most_common_words(sys.path[0] + '/data/lorem_ipsum.txt'))
```

### Task 3
Common code:
```python
from collections import namedtuple
from typing import List, Tuple
import csv
import sys


def read_csv(file_path: str) -> List[Tuple[str, int, float]]:
    path = sys.path[0] + file_path
    Student = namedtuple('Student', ['name', 'age', 'avg'])

    try:
        with open(path, 'r') as file:
            res = []
            for student in map(Student._make, csv.reader(file)):
                res.append(student)

        return res
    except FileNotFoundError:
        print(f"No file at {path}")

...
...
...

if __name__ == '__main__':
    print(get_top_performers('/data/students.csv'))
    write_csv_by_age('/data/students.csv', '/data/sorted_students.csv')
```
1) Implement a function which receives file path and returns names of top performer students
```python
def get_top_performers(file_path: str, number_of_top_students: int = 5) -> list:
    res = read_csv(file_path)
    students = sorted(res[1:], reverse=True, key=lambda arg: float(arg.avg))

    return [student[0] for student in students[:number_of_top_students]]
```

2) Implement a function which receives the file path with student info and writes CSV student information to the new file in descending order of age.
```python
def write_csv_by_age(source: str, destination: str, desc=True) -> None:
    res = read_csv(source)

    path = sys.path[0] + destination
    try:
        with open(path, 'w') as file:
            writer = csv.writer(file)
            writer.writerow(res[0])  # Header
            for student in sorted(res[1:], reverse=desc, key=lambda st: st.age):
                writer.writerow(student)
    except FileNotFoundError:
        print(f'No file at {destination}')
```
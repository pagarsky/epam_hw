import sys


def sort_names(source: str, destination: str) -> None:
    try:
        with open(source, 'r') as file:
            names = file.readlines()
            names.sort()

        with open(destination, 'w') as dest:
            dest.writelines(names)

    except FileNotFoundError:
        print(f'No file found at {source} or {destination}')


if __name__ == '__main__':
    sort_names(sys.path[0] + '/data/unsorted_names.txt', sys.path[0] + '/data/sorted_names.txt')

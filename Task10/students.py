from collections import namedtuple
from typing import List, Tuple
import csv
import sys


def read_csv(file_path: str) -> List[Tuple[str, int, float]]:
    path = sys.path[0] + file_path
    Student = namedtuple('Student', ['name', 'age', 'avg'])

    try:
        with open(path, 'r') as file:
            res = []
            for student in map(Student._make, csv.reader(file)):
                res.append(student)

        return res
    except FileNotFoundError:
        print(f"No file at {path}")


def get_top_performers(file_path: str, number_of_top_students: int = 5) -> list:
    res = read_csv(file_path)
    students = sorted(res[1:], reverse=True, key=lambda arg: float(arg.avg))

    return [student[0] for student in students[:number_of_top_students]]


def write_csv_by_age(source: str, destination: str, desc=True) -> None:
    res = read_csv(source)

    path = sys.path[0] + destination
    try:
        with open(path, 'w') as file:
            writer = csv.writer(file)
            writer.writerow(res[0])  # Header
            for student in sorted(res[1:], reverse=desc, key=lambda st: st.age):
                writer.writerow(student)
    except FileNotFoundError:
        print(f'No file at {destination}')


if __name__ == '__main__':
    print(get_top_performers('/data/students.csv'))
    write_csv_by_age('/data/students.csv', '/data/sorted_students.csv')

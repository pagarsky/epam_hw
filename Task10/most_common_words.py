from collections import Counter
import sys


def most_common_words(filepath: str, number_of_words: int = 3) -> list:
    try:
        words = Counter()
        with open(filepath, 'r') as file:
            for line in file.readlines():
                words.update(word.strip('.,').lower() for word in line.split())

        return [tup[0] for tup in words.most_common(number_of_words)]
    except FileNotFoundError:
        print(f'File not found at {filepath}')


if __name__ == '__main__':
    print(most_common_words(sys.path[0] + '/data/lorem_ipsum.txt'))

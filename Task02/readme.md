#### Exercise 1
Calculate negative numbers in a given list:

* Note: do not use conditionals or loops

``` python
def count_negatives(numbers: list) -> int:  
    return str(numbers).count('-')


if __name__ == '__main__':
    lst = [4, -9, 8, -11, 8]
    print(count_negatives(lst))
```

#### Exercise 2

Swap first and the last players in a given list:

```python
def change_positions(players: list) -> None:
    players[0], players[-1] = players[-1], players[0]
    print(players)


if __name__ == '__main__':
    players = ['Ashleigh Barty', 'Simona Halep', 'Naomi Osaka', 'Karolina Pliskova', 'Elina Svitolina']
    change_positions(players)
```

#### Exercise 3

Swap words "reasonable" and "unreasonable" in a given quote:
* Note: do not use <string>.replace() function or similar

```python
def swap_words(string: str, word1: str, word2: str) -> str:
    lst = string.split(' ')

    index1 = lst.index(word1)
    index2 = lst.index(word2)

    lst[index1], lst[index2] = lst[index2], lst[index1]

    return ' '.join(lst)


if __name__ == '__main__':
    quote = "The reasonable man adapts himself to the world; " \
            "the unreasonable one persists in trying to adapt the world to himself."

    new_quote = swap_words(quote, 'reasonable', 'unreasonable')

    print(new_quote)
```
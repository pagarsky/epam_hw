def count_negatives(numbers: list) -> int:
    return str(numbers).count('-')


if __name__ == '__main__':
    lst = [4, -9, 8, -11, 8]
    print(count_negatives(lst))


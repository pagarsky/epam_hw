def swap_words(string: str, word1: str, word2: str) -> str:
    lst = string.split(' ')

    index1 = lst.index(word1)
    index2 = lst.index(word2)

    lst[index1], lst[index2] = lst[index2], lst[index1]

    return ' '.join(lst)


if __name__ == '__main__':
    quote = "The reasonable man adapts himself to the world; " \
            "the unreasonable one persists in trying to adapt the world to himself."

    new_quote = swap_words(quote, 'reasonable', 'unreasonable')

    print(new_quote)
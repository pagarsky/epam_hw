### Task 1
Look through file modules/legb.py.  
1\) Find a way to call inner_function without moving it from inside of enclosed_function.  
```python
import Task11.modules.legb as legb


if __name__ == '__main__':

    # task 1
    exec(legb.enclosing_funcion.__code__.co_consts[2])
```

2.1) Modify ONE LINE in inner_function to make it print variable 'a' from global scope.  

```python
a = "I am global variable!"


def enclosing_funcion():
    a = "I am variable from enclosed function!"

    def inner_function():

        a = "I am local variable!"
print(a)                            # we can just remove 2 tabulations

```

2.2) Modify ONE LINE in inner_function to make it print variable 'a' form enclosing function.  
```python
a = "I am global variable!"


def enclosing_funcion():
    a = "I am variable from enclosed function!"

    def inner_function():

        a = "I am local variable!"
    print(a)                        # same trick but with one tabulation


enclosing_funcion()
```


### Task 2
Implement a decorator remember_result which remembers last
result of function it decorates and prints it before next call.

```python
def remember_result(function):
    last_call = None

    def inner(*args):
        nonlocal last_call
        
        print(f'Last result = {last_call}')
        last_call = function(*args)

    return inner


@remember_result
def sum_list(*args):
    result = ''.join(str(arg) for arg in args)
    print(f'Current result = {result}')
    return result


if __name__ == '__main__':
    sum_list('a', 'b')
    sum_list('a', 'b', 'c')
    sum_list('aaa', 'bsss', 'cssec')
```

### Task 3
Implement a decorator call_once which runs a function or method
once and caches the result. All consecutive calls to this function should
return cached result no matter the arguments.

```python
from functools import reduce


def call_once(func):
    cache = {}

    def inner(*args):
        nonlocal cache

        if func in cache:
            return cache[func]

        cache[func] = func(*args)
        return cache[func]

    return inner


@call_once
def sum_of_numbers(a, b):
    return a + b


if __name__ == '__main__':
    print(sum_of_numbers(13, 42))
    print(sum_of_numbers(22, 22))
    print(sum_of_numbers(999, 100))
    print(sum_of_numbers(0, 12132))
```

### Task 4
1) The output is:
    ```
    >>> 5
    ```
    Because in mod_a we import mod_c and print the x values fom it
2) The output is:
    ```
    >>> [1, 2, 3]
    ```
   Because x is a 'label'(as all python variables) we can change it to list without any problems
   
3) When we change import statement to 'from mod_c import *' there is error, because we didn't define name 'mod_c'


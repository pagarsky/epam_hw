from functools import reduce


def call_once(func):
    cache = {}

    def inner(*args):
        nonlocal cache

        if func in cache:
            return cache[func]

        cache[func] = func(*args)
        return cache[func]

    return inner


@call_once
def sum_of_numbers(a, b):
    return a + b


if __name__ == '__main__':
    print(sum_of_numbers(13, 42))
    print(sum_of_numbers(22, 22))
    print(sum_of_numbers(999, 100))
    print(sum_of_numbers(0, 12132))

def remember_result(function):
    last_call = None

    def inner(*args):
        nonlocal last_call
        
        print(f'Last result = {last_call}')
        last_call = function(*args)

    return inner


@remember_result
def sum_list(*args):
    result = ''.join(str(arg) for arg in args)
    print(f'Current result = {result}')
    return result


if __name__ == '__main__':
    sum_list('a', 'b')
    sum_list('a', 'b', 'c')
    sum_list('aaa', 'bsss', 'cssec')

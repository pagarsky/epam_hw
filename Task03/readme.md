### Task 3
##### Exercise 1
* Write a program tha determines wheter the Point(x, y) is in the shaded area

```python
def validate_point(x: float , y: float) -> bool:
    return 0 <= y <= 1 and y >= abs(x)


if __name__ == '__main__':
    x = float(input("Enter x: "))
    y = float(input("Enter y: "))

    print("Is point in shadow area: ", validate_point(x, y))

```

##### Exercise 2
* Write a program that prints the input number from 1 to 100.  
But for multiple of three print "Fizz" instead of the number and for the multiples of five print "Buzz". For number whuch are multiples of both three and five print "FizzBuzz". 

```python
def fizz_buzz(number: int) -> None:
    if not 1 <= number <= 100:
        print(f'{number} is out of range [1, 100]')
        return

    s = []
    if number % 3 == 0:
        s.append('Fizz')
    if number % 5 == 0:
        s.append('Buzz')
    if len(s) == 0:
        print(number)
        return

    print(''.join(s))


if __name__ == '__main__':
    while True:
        s = input('Enter the number(q to quit): ')
        if s in 'qQ':
            break

        n = int(s)
        fizz_buzz(n)
```

##### Exercise 3
* Simulate the one round of play for baccarat game

```python
def calculate(card1: str, card2: str) -> int:
    return (values[card1] + values[card2]) % 10


if __name__ == '__main__':
    values = {'A': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9,
              '10': 0, 'J': 0, 'Q': 0, 'K': 0}
    first = input('Enter first card: ')
    second = input('Enter second card: ')

    s = ['Your result:']

    if first not in values or second not in values:
        s.append('Do not cheat!')
    else:
        s.append(str(calculate(first, second)))
    print(' '.join(s))
```
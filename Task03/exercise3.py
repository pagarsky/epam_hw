def calculate(card1: str, card2: str) -> int:
    return (values[card1] + values[card2]) % 10


if __name__ == '__main__':
    values = {'A': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9,
              '10': 0, 'J': 0, 'Q': 0, 'K': 0}
    first = input('Enter first card: ')
    second = input('Enter second card: ')

    s = ['Your result:']

    if first not in values or second not in values:
        s.append('Do not cheat!')
    else:
        s.append(str(calculate(first, second)))
    print(' '.join(s))

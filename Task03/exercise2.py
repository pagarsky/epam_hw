def fizz_buzz(number: int) -> None:
    if not 1 <= number <= 100:
        print(f'{number} is out of range [1, 100]')
        return

    s = []
    if number % 3 == 0:
        s.append('Fizz')
    if number % 5 == 0:
        s.append('Buzz')
    if len(s) == 0:
        print(number)
        return

    print(''.join(s))


if __name__ == '__main__':
    while True:
        s = input('Enter the number(q to quit): ')
        if s in 'qQ':
            break

        n = int(s)
        fizz_buzz(n)

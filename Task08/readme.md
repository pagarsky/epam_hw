***
![Task 1](./Capture.PNG)

If we pass value to a that is greater than 7, the assert is going to trigger.  
Also the ValueError is going to be raised if we pass some value to input which can't be converted to int

***
![Task 2](./Capture1.PNG)

If we assume, that bar() and x are defined somewhere above this code snippet, then:
```python
after bar
or this after bar?
``` 
is the output.
***
![Task 3](./Capture2.PNG)

This code snippet will print 2

***
![Task 4](./Capture3.PNG)

This will print 1.

***
![Task 5](./Capture4.PNG)

This code snippet will print nothing, because we can't raise "Error", we need it to derive from Exception class, so this will raise TypeError.

***
![Task 6](./Capture5.PNG)

On every iteration:  
If user enters valid path it will read it without any errors.  
But if path will be incorrect this will be printed
```python
Input File Not Found
```
***